# Social media analysis in Twitter on tweets related to ASD in 2019-2020 with particular attention to covid-19: topic modelling and sentiment analysis

Twitter is one of the most significant digital platforms for sharing relevant information. The goal is to analyze the messages posted on this micro blogging platform regarding Autism Spectrum Disorder (ASD). The primary objective is to deepen the topics covered within the various tweets, to understand the attitude of the various people interested in the topic. In particular, we will focus on the discussion of ASD and covid-19.

# Data collection
The data collection process was based on the search for tweets through hashtags and keywords, in particular: #autism; #ASD; #ActuallyAutistic; #AutismSpeaks; #AutismParent; #Autchat; #AutismAwareness; #WorldAutismAwarenessDay; aspergers; autism; autistic; autism sprectrum; Disorder; spectrumDisorder; autism disorder.
This serch was performed using web scraping and all subsequent manipulation of the collected data was performed through the use of the Twitter search application programming interface ([API](https://developer.twitter.com/en/docs/twitter-api)).

# Bot identification
To identify bots [Botometer](https://botometer.osome.iu.edu/) was used: You will need to identify yourself using credentials and set a threshold for identifying bots, depending on your purposes. For this reason it is important to rely on the distribution of the values returned by Botometer since set some arbitrary threshold score is not recommend. 

# Data analysis
For topic modelling different possible approaches were tested, but the NMF (Non-Negative Matrix Factorization) method was used in this study because it produces more coherent topics compared to other solutions. 
NMF reduce the dimension of the input corpora: each of the words in the document are given a weight based on the semantic relationship between the words, but the one with highest weight is considered as the topic for a set of words.
Pre-processing was necessary before applying the NMF in order to create the document term matrix with TfidfVectorizer and fitting and transforming the model.

Sentiment analysis was rule-based and AFiNN, a lexicon that includes 2477 words and uses a scoring system in which each word is assigned a value from -5 (very negative) to +5 (very positive), was used. Words with a score below -0.05 are identified as negative, while those with a score above 0.05 are identified as positive, Neutral otherwise.
